# KLab
KLab is small Matlab language interpreter written completly in Java 8
The applications supports only basic Matlab operations.




## To compile requires:
- Java 8
- Maven

# Data types
## Scalar types
double:
``` 
a=2+3i;
```

## String
To create a string use two apostrophes:
```
string_variable = 'Hello world'
```

## Matrix
To create a matrix use fallowing instruction:
```
A = [1 3 -3 ; 1+10 3*10 3 ; [3 3 2]]
%or
A = [ 1 3 -3
    1+10 3*10 3
    [3 3 2]]
```

# Syntax
## Command entering
Instructions can be separated by semicolon, comma or new line:
```
 a=3+2i , b=2; d= 2 + 3*10;
```
To prevent command result display use semicolon.

## IF instruction

```
 if (a==1)
    c=2
 elseif (3)
    d=2
 else
    2
 end
```
## Loops
```
A=[]; 
for i=1:0.1:10
  A(1,i)=i*i;
end

```

Inside loops 'break' and 'continue' keywords might be used.

## Functions
Function must be declered in seperate ".m" file. The file name must be the same as a function name. File has to start with function declaration (see fallowing example).
```
 function [out1,out2] = methodName(in1,in2,in3) 
    out1=in1*in2;
    out2=in1+in3;
 end
```
To exit function or script use 'return' keyword.


# Fast vector genarating
```
i = 1:10; % default step is always 1
ii = 1:0.1:10;
```

# Matrix connection
```
>> A = [1 ; 2 ; 3];
>> B = [ A [4 3; 5 4 ; 6 5] ]
B=
1  4   3
2  5   4
3  6   5

>> C = [ A ; [4 3; 5 4 ; 6 5] ]
Col: 28, Line: 1 Dimensions of matrices being concatenated are not consistent.
>> C = [ [ A [10 ; 11 ; 12-3 ] ] ; [4 3; 5 4 ; 6 5] ]
C=
1   10
2   11
3   9
4   3
5   4
6   5
```
# Matrix generating methods
```
rand(m)
rand(m,n)
eye(m,n)
ones(m,n)
zeros(m,n)
```

# Matrix indexing
```
 A(i,j)
 A(i) (matrix A treated as column vector)
 A(k:m,j)
 A(:,j)
 A(i,:)
 A(end,i)
 A(end+3,j)=10; (automatic matrx expansion)
 ```


# Arithmetic operators
```
 A+B
 +A
 A-B
 -A
 A*B
 A.*B
 A/B (compute A*inv(B))
 A./B
 A\B (solves A*x=B)
 A.\B
 A^B
 A.^B
 A'
 A.'
```
# Relational operatos
```
 A==B
 A~=B
 A<B
 A<=B
 A>B
 A>=B
```
# Logical operators
```
 expression1 | expression2
 expression1 & expression2
 expression1 || expression2 %(short-circuit OR operator, applied only to scalar arguments)
 expression2 && expression2 %(short-circuit AND operator)
 ~negation
```
# Build in functions
 ```
 a=sqrt(A)
 a=log(b) or a=log(b,c)
 a=log2(c)
 a=log10(d)
 sin(A)
 cos(A)
 tan(A)
 cot(A)
 conj(3+2i) == 3-2i 
 det(A)
 inv(A) == A^-1
 x=url(A,b)
 [L,U,P]=lu(A)
```
# Matrix sizes
```
 [m,n]=size(A)
 a=length(C)
 numel(A)
 iscolumn(A)
 isrow(A)
 isempty(A)
 isscalar(A)
 isvector(A)
 rows(A)
 cols(A)
```
# Data plots
KLab can draw 2D and 3D plots using fallowing methods:
```
 plot(x,y)
 plot3(x,y,z)
```

More information available at (PL version):
https://drive.google.com/file/d/0ByCTzDu-eLGEZzktQnBhRFRYVFE/view?usp=sharing